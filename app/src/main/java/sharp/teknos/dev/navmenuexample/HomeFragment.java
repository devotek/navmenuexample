package sharp.teknos.dev.navmenuexample;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements View.OnClickListener{


    private EditText nameEditText;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        nameEditText = (EditText)view.findViewById(R.id.nameEditText);
        Button saveBtn = (Button)view.findViewById(R.id.saveBtn);
        saveBtn.setOnClickListener(this);

        nameEditText.setText(Util.getString(getContext(),Util.NAME));

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        Util.saveString(getContext(),Util.NAME,nameEditText.getText().toString());
    }
}
