package sharp.teknos.dev.navmenuexample;


import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment implements View.OnClickListener {


    private EditText nameEditText;
    private EditText subjectEditText;
    private MyDatabase myDatabase;
    private SimpleCursorAdapter simpleCursorAdapter;
    private ListView nameListView;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        myDatabase = new MyDatabase(context);
        myDatabase.openDb();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        nameEditText = (EditText)view.findViewById(R.id.nameEditText);
        subjectEditText = (EditText)view.findViewById(R.id.subjectEditText);
        nameListView = (ListView)view.findViewById(R.id.nameListView);


        refreshAdapter();


        Button saveBtn = (Button)view.findViewById(R.id.saveBtn);
        saveBtn.setOnClickListener(this);
        return view;
    }

    private void refreshAdapter(){
        Cursor cursor = myDatabase.getAllStudents();
        simpleCursorAdapter = new SimpleCursorAdapter(getContext(),
                R.layout.list_item,
                cursor,
                new String[]{MyDatabase.FIRST_NAME,MyDatabase.SUBJECT},
                new int[]{R.id.fisrtNameText,R.id.subjectText},1);
        nameListView.setAdapter(simpleCursorAdapter);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        myDatabase.closeDb();
    }

    @Override
    public void onClick(View v) {
        if (nameEditText.getText().toString().isEmpty() && subjectEditText.getText().toString().isEmpty()){

        }else {

            myDatabase.addStudent(nameEditText.getText().toString(), subjectEditText.getText().toString());
            refreshAdapter();
/*
            Cursor cursor = myDatabase.getAllStudents();
            cursor.moveToFirst();
            for (int i = 0; i < cursor.getCount(); i++) {
                String name = cursor.getString(cursor.getColumnIndex(MyDatabase.FIRST_NAME));
                Log.w("NAME", name);
                cursor.moveToNext();
            }
            cursor.close();
*/
            nameEditText.setText("");
            subjectEditText.setText("");
        }

    }
}
