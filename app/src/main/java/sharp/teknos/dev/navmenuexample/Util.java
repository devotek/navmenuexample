package sharp.teknos.dev.navmenuexample;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Util {

    public static final String NAME = "firstname";

    public static void saveString(Context context,String key, String value){
        SharedPreferences.Editor ed = PreferenceManager.getDefaultSharedPreferences(context).edit();
        ed.putString(key,value);
        ed.commit();
    }

    public static String getString(Context context, String key){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(key,"");
    }

}
