package sharp.teknos.dev.navmenuexample;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{


    private DrawerLayout drawerLayout;
    private LinearLayout menuLayout;
    private String[] menuItems = new String[]{"Home","Profile","Feedback"};
    private FragmentTransaction fragmentTransaction;
    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("NavList Example");
        actionBar.setDisplayHomeAsUpEnabled(true);
        drawerLayout = (DrawerLayout)findViewById(R.id.drawerLayout);
        menuLayout = (LinearLayout)findViewById(R.id.menuLayout);
        ListView menuListView = (ListView)findViewById(R.id.menuList);
        menuListView.setOnItemClickListener(this);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,
                drawerLayout,
                R.string.open_drawer,
                R.string.close_drawer);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,menuItems);
        menuListView.setAdapter(adapter);

        fragmentManager = getSupportFragmentManager();

        loadFragment(0);


    }

    private void loadFragment(int position){

        switch (position){
            case 0:
                //Home
                HomeFragment homeFragment = new HomeFragment();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.mainFrameView,homeFragment);
                fragmentTransaction.commit();
                break;

            case 1:
                //Profile
                ProfileFragment profileFragment = new ProfileFragment();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.mainFrameView,profileFragment);
                fragmentTransaction.commit();
                break;

            case 2:
                //Feedback
                FeedbackFragment feedbackFragment = new FeedbackFragment();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.mainFrameView,feedbackFragment);
                fragmentTransaction.commit();
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                if (drawerLayout.isDrawerOpen(menuLayout)){
                    drawerLayout.closeDrawer(menuLayout);
                }else{
                    drawerLayout.openDrawer(menuLayout);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        loadFragment(position);
        drawerLayout.closeDrawer(menuLayout);
    }
}
