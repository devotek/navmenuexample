package sharp.teknos.dev.navmenuexample;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MyDatabase {

    public static final String DB_NAME = "mydb.db";
    public static final int DB_VER = 1;
    public static final String MY_TBL = "student_tbl";
    public static final String FIRST_NAME = "first_name";
    public static final String SUBJECT = "subject";
    public static final String ID = "_id";

    public static final String CREATE_TBL = "CREATE TABLE "+MY_TBL+" ("+
            ID+" INTEGER PRIMARY KEY AUTOINCREMENT,"+
            FIRST_NAME+" TEXT NOT NULL,"+
            SUBJECT+" TEXT NOT NULL)";

    public static final String[] COLUMNS = new String[]{ID,FIRST_NAME,SUBJECT};

    private MyDBOpenHelper myDBOpenHelper;
    private SQLiteDatabase sqLiteDatabase;

    public MyDatabase(Context context){
        myDBOpenHelper = new MyDBOpenHelper(context);
    }

    public void openDb(){
        sqLiteDatabase = myDBOpenHelper.getWritableDatabase();
    }

    public void closeDb(){
        sqLiteDatabase.close();
        sqLiteDatabase = null;
    }

    public void addStudent(String name,String subject){
        ContentValues contentValues = new ContentValues();
        contentValues.put(FIRST_NAME,name);
        contentValues.put(SUBJECT,subject);
        sqLiteDatabase.insert(MY_TBL,null,contentValues);
    }

    public void editStudent(long id, String name,String subject){
        ContentValues contentValues = new ContentValues();
        contentValues.put(FIRST_NAME,name);
        contentValues.put(SUBJECT,subject);
        sqLiteDatabase.update(MY_TBL,contentValues,ID+" = ?",new String[]{Long.toString(id)});
    }

    public void deleteStudentById(long id){
        sqLiteDatabase.delete(MY_TBL,ID+" = ?", new String[]{Long.toString(id)});
    }

    public Cursor getAllStudents(){
        Cursor cursor = sqLiteDatabase.query(MY_TBL,COLUMNS,null,null,null,null,null);
        return cursor;
    }

    public Cursor getStudentByID(long id){
        Cursor cursor = sqLiteDatabase.query(MY_TBL,COLUMNS,ID+" = ?",new String[]{Long.toString(id)},null,null,null);
        return cursor;
    }



    private class MyDBOpenHelper extends SQLiteOpenHelper{

        public MyDBOpenHelper(Context context) {
            super(context, DB_NAME, null, DB_VER);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TBL);

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            if (newVersion > oldVersion){
                db.execSQL("DROP "+MY_TBL);
                onCreate(db);
            }
        }
    }
}
